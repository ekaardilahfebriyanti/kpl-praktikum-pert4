package main;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.*;
import java.util.HashMap;

import static spark.Spark.get;

public class App {
    public static void main(String[] args) {
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict",
                true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader")
        ;
        VelocityTemplateEngine velocityTemplateEngine = new VelocityTemplateEngine(configuredEngine);
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("AES");
            KeyGenerator keyGenerator =
                    KeyGenerator.getInstance("AES");
            SecureRandom secureRandom = new SecureRandom();
            int keyBitSize = 256;
            keyGenerator.init(keyBitSize, secureRandom);
            SecretKey secretKey = keyGenerator.generateKey();
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            e.printStackTrace();
        }
        Connection c;
        Statement statement;
        try {
//            Class.forName("org.hsqldb.jdbc.JDBCDriver");
//            Class.forName("com.mysql.jdbc.Driver");
            Class.forName("com.mysql.cj.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://localhost/user_kpl", "root", "");
//                    "jdbc:hsqldb:mem:usersdb", "SA", "");
            DatabaseMetaData dbm = c.getMetaData();
            ResultSet rs = dbm.getTables(null, null, "users", null);
            statement = c.createStatement();
//            statement.execute("CREATE TABLE users(username varchar(255), password varchar(255));");
            byte[] plainText = "admin".getBytes("UTF-8");
            byte[] cipherText = cipher.doFinal(plainText);
            String s = new String(cipherText);
            if (rs.next()) {
                statement.execute("UPDATE users SET password = '"+s+"' WHERE username='admin'");
            }else {
                statement.execute("CREATE TABLE users(username varchar(255), password varchar(255));");
                statement.execute("INSERT INTO users (`username`, `password`) values ('admin', '"+s+"')");
            }

        } catch (Exception e) {
            System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
            e.printStackTrace();
            return;
        }
        Cipher cipher1 = cipher;
        get("/login", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String templatePath = "/views/login.vm";
            String username = req.queryParamOrDefault("username", "");
            String password = req.queryParamOrDefault("password", "");
            byte[] plainText = password.getBytes("UTF-8");
            byte[] cipherText = cipher1.doFinal(plainText);
            password = new String(cipherText);

//            statement.execute("INSERT INTO users values ('admin', 'admin')");
//        } catch (Exception e) {
//            System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
//                    e.printStackTrace();
//            return;
//        }
//        get("/login", (req, res) -> {
//            HashMap<String, Object> model = new HashMap<>();
//            String templatePath = "/views/login.vm";
//            String username = req.queryParamOrDefault("username", "");
//            String password = req.queryParamOrDefault("password", "");
//            if (username.isEmpty() && password.isEmpty()) {
//                model.put("message", "");
//            } else {
//                String sql = "SELECT * FROM users WHERE username=? AND password=?";
//                PreparedStatement preparedStatement = c.prepareStatement(sql);
//                preparedStatement.setString(1, username);
//                preparedStatement.setString(2, password);
//                try {
////                    ResultSet resultSet = statement.executeQuery(sql);
//                    ResultSet resultSet = preparedStatement.executeQuery();
//                    if (resultSet.next()) {
//                        model.put("message", "Login berhasil");
//                    } else {
//                        model.put("message", "Login gagal");
//                    }
//                } catch (Exception e) {
//                    System.err.println("ERROR: gagal mengeksekusi query.");
//                            model.put("message", "Login gagal");
//                }
//            }
            if (username.isEmpty() && password.isEmpty()) {
                model.put("message", "");
            } else {
                String sql = "SELECT * FROM users WHERE `username`=? and `password`=?";

                PreparedStatement preparedStatement =
                        c.prepareStatement(sql);
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, password);
//                model.put("ps", password);

                try {
                    ResultSet resultSet =
                            preparedStatement.executeQuery();
                    if (resultSet.next()) {
                        model.put("message", "Login berhasil");
                    } else {
                        model.put("message", "Login gagal");
                    }
                } catch (Exception e) {
                    System.err.println("ERROR: gagal mengeksekusi query.");
                            model.put("message", "Login gagal");
                    e.printStackTrace();
                }
            }
            return velocityTemplateEngine.render(new
                    ModelAndView(model, templatePath));
        });
    }
}
